package com.aiconoa.trainings.jsf;

import java.io.Serializable;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Range;

import com.aiconoa.trainings.ejb.RandomNumberEJB;

@Named
@ViewScoped
public class GuessANumberBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = Logger.getLogger(GuessANumberBean.class.getName());
	private Integer numberToGuess;

	@NotNull
	@Range(min=1, max=10)
	private Integer submitedNumber;

	private boolean result;
	
	@Inject
	private RandomNumberEJB randomNumberEJB;
	
//	public GuessANumberBean() {
//		
//	}
	
	@PostConstruct
	public void init() {
		numberToGuess = randomNumberEJB.generateRandomInteger(10);
	}
	
	
	public Integer getSubmitedNumber() {
		LOGGER.info(String.format("getSubmitedNumber - %s", FacesContext.getCurrentInstance().getCurrentPhaseId()));
		return submitedNumber;
	}
	
	public void setSubmitedNumber(Integer submitedNumber) {
		LOGGER.info(String.format("setSubmitedNumber - %s", FacesContext.getCurrentInstance().getCurrentPhaseId()));
		this.submitedNumber = submitedNumber;
	}
	
	public void guess() {
		LOGGER.info("number to guess is " + numberToGuess);
		LOGGER.info(String.format("guess - %s", FacesContext.getCurrentInstance().getCurrentPhaseId()));
		result = (submitedNumber.equals(numberToGuess));
	}
	
	public boolean getResult() {
		LOGGER.info(String.format("getResult - %s", FacesContext.getCurrentInstance().getCurrentPhaseId()));
		return result;
	}
	
}
