package com.aiconoa.trainings.jsf;

import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

@Named
@RequestScoped
public class QueryBean {

	private Integer id;
	
	public void setId(Integer id) { // phase Update Model Values - 4
		this.id = id;
	}
	
	public Integer getId() {
		return id;
	}

	public String load() { // phase Invoke Application - 5
		// effectuer le traitement requis, ici this.id est disponible
		// et correspond à ce qui était dans le id de la queryString
		
		return "hello-jsf"; // redirection sur hello-jsf
	}
	
//	public void load() { // phase Invoke Application - 5
//		// effectuer le traitement requis, ici this.id est disponible
//		// et correspond à ce qui était dans le id de la queryString
//		
//		FacesContext.getCurrentInstance()
//					.getExternalContext().redirect(url);
//	}
	
}
