package com.aiconoa.trainings.cdi;

import java.util.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;

@ApplicationScoped
public class MyEventObserver {
	
	
	@Logged
	public void listenToMyEvent(@Observes MyEvent event) {
	}
}
