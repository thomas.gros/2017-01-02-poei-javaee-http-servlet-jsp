package com.aiconoa.trainings.ejb;

import java.util.Random;

import javax.ejb.Stateless;

@Stateless
public class RandomNumberEJB {

	/**
	 * Generates a random number between 0 and max (exclusive)
	 * @param max
	 * @return
	 */
	public int generateRandomInteger(int max) {
		return new Random().nextInt(max);
	}
	
}
