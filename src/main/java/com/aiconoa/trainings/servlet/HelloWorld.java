package com.aiconoa.trainings.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class HelloWorld
 */
@WebServlet("/HelloWorld")
public class HelloWorld extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public HelloWorld() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setCharacterEncoding("UTF-8");

		response.addHeader("Content-Type", "text/html");
		
//		response.getWriter()
//		.append("Served at: ")
//		.append(request.getContextPath());
		
		response.getWriter()
		.append("<!DOCTYPE html>")
		.append("<html>")
			.append("<head>")
				.append("<meta charset=\"UTF-8\">")
			.append("</head>")
			.append("<body>")
				.append("<p>Hello World</p>")
				.append("<p>é è ã</p>")
				.append("<br>")
				.append("<a href=\"AnotherServlet\">vers AnotherServlet</a>")
				.append("<br>")
				.append("<a href=\"http://localhost:8080/http-jsp-servlet/AnotherServlet\">vers AnotherServlet</a>")
			.append("</body>")
		.append("</html>");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
