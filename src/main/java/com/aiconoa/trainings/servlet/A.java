package com.aiconoa.trainings.servlet;

public class A {
//	@Inject
	private B b;
	
//	@Inject
//	public A(B b) {
//		this.b = b;
//	}
	
	//@Inject
	public void setB(B b) {
		this.b = b;
	}
	
	
	public void doSomethingA() {
		if(b != null) {
			b.doSomethingB();
		}
	}
	
}
