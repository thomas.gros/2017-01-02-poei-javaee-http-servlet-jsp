package com.aiconoa.trainings.cdi;

import java.util.Date;

public class LampOnOffEvent {

	private Lamp lamp;
	private Date date;
	
	public LampOnOffEvent(Lamp lamp, Date date) {
		this.lamp = lamp;
		this.date = date;
	}

	@Override
	public String toString() {
		return "LampOnOffEvent [lamp=" + lamp + ", date=" + date + "]";
	}

}