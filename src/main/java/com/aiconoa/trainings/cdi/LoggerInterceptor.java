package com.aiconoa.trainings.cdi;

import java.util.logging.Logger;

import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

@Logged
@Interceptor
public class LoggerInterceptor {

	@AroundInvoke
	public Object aroundInvocation(InvocationContext invocationContext) throws Exception {
		
		Logger logger = Logger.getLogger(invocationContext.getMethod().getDeclaringClass().getName());
		logger.info(String.format("Called %s ", invocationContext.getMethod().getParameters()));
		
		return invocationContext.proceed();
	}
	
}
