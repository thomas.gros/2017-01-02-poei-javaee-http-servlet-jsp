package com.aiconoa.trainings.cdi;

public class MyEvent {

	private String data;

	public MyEvent(String data) {
		super();
		this.data = data;
	}

	public String getData() {
		return data;
	}

	@Override
	public String toString() {
		return "MyEvent [data=" + data + "]";
	}

}
