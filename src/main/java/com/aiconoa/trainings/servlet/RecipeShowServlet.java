package com.aiconoa.trainings.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class RecipeShowServlet
 */
@WebServlet("/RecipeShowServlet")
public class RecipeShowServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
 

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		String recipeId = request.getParameter("id");
		// TODO vérifier la validité du paramètre
		
		// TODO vérifier que l'utilisateur ayant émis la requête est bien l'auteur de la recette
		
		// TODO vérifier si un sessionID existe en cookie
			// si non présent => rediriger vers la page /LogInServlet
			// si oui
				// vérifier si ce cookie de session correspond à l'auteur de la recette
					// si non => afficher page "forbidden"
					// si oui => afficher la page de la recette
		
		
		
		// TODO chercher la recipe dans la BDD et la passer dans request.setAttributes
		request.getRequestDispatcher("/WEB-INF/recipe-show.jsp").forward(request, response);
	}

}
