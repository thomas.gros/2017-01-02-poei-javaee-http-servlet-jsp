package com.aiconoa.trainings.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

/**
 * Servlet implementation class JDBCServlet
 */
@WebServlet(urlPatterns={"/JDBCServlet"})
public class JDBCServlet extends HttpServlet {	
	private static final long serialVersionUID = 1L;
	
	private static final Logger LOGGER = Logger.getLogger(JDBCServlet.class.getName()); 

	@Resource(lookup="java:jboss/DataSources/RecipeDS")
	private DataSource recipeDS;
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		Connection conn = null;
		
//		try {
//			conn = recipeDS.getConnection();
//			
//			PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM recipe");
//			
//			ResultSet rs = pstmt.executeQuery();
//			
//			while(rs.next()) {
//				LOGGER.info(
//						String.format("Found recipe: %d %s %s %s", 
//										rs.getInt("id"), 
//										rs.getString("title"), 
//										rs.getString("thumbnail"), 
//										rs.getString("description")));
//				
//				// remplir la réponse
//			}
//			
//			
//		} catch (SQLException e) {
//			LOGGER.log(Level.SEVERE, "Error communicating with the database", e);
//			response.sendError(500, "Something wrong happened, please contact the support.");
//			return;
//		} finally {
//			if(conn != null) {
//				try {
//					conn.close();
//				} catch (SQLException e) {
//					LOGGER.log(Level.SEVERE, "Error closing the connection with the database", e);
//				}
//			}
//		}
		
		try (Connection conn = recipeDS.getConnection()) { // Java SE 7, try-with-resources
			
			PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM recipe");
			
			ResultSet rs = pstmt.executeQuery();
			
			while(rs.next()) {
				LOGGER.info(
						String.format("Found recipe: %d %s %s %s", 
										rs.getInt("id"), 
										rs.getString("title"), 
										rs.getString("thumbnail"), 
										rs.getString("description")));
				
				// remplir la réponse
			}
			
			
		} catch (SQLException e) {
			LOGGER.log(Level.SEVERE, "Error communicating with the database", e);
			response.sendError(500, "Something wrong happened, please contact the support.");
			return;
		}
		
		
		
	}

}
