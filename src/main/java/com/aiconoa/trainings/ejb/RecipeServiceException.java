package com.aiconoa.trainings.ejb;

import javax.ejb.ApplicationException;

@ApplicationException(rollback=true)
public class RecipeServiceException extends RuntimeException {

	private String column;
	private String type;
	
	public RecipeServiceException() {
		super();
	}

	public RecipeServiceException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public RecipeServiceException(String message, Throwable cause) {
		super(message, cause);
	}

	public RecipeServiceException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public RecipeServiceException(Throwable cause) {
		super(cause);
	}
	
	public RecipeServiceException(String column, String type, Throwable cause) {
		super(cause);
		this.column = column;
		this.type = type;
	}
	
	public String getColumn() {
		return column;
	}
	
	public String getType() {
		return type;
	}

}
