package com.aiconoa.trainings.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class DetailItemServlet
 */
@WebServlet("/DetailItemServlet")
public class DetailItemServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
  
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String indexQueryString = request.getParameter("index");
		
		if(indexQueryString == null) {
			response.sendError(HttpServletResponse.SC_NOT_FOUND, "Page not found");
			return;
		}
		
		if(! indexQueryString.matches("^\\d+$")) { // only digits are allowed
			response.sendError(HttpServletResponse.SC_NOT_FOUND, "Page not found");
			return;
		}
		
		int index = 0;
		try {
			index = Integer.parseInt(indexQueryString);
		} catch (NumberFormatException nfe) {
			response.sendError(HttpServletResponse.SC_NOT_FOUND, "Page not found");
			return;
		}
		
		// récupérer l'item associé à l'index
		
		
	}

}
