package com.aiconoa.trainings.servlet;

import java.io.IOException;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class LogInServlet
 */
@WebServlet("/LogInServlet")
public class LogInServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LogInServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// presenter un formulaire de connexion
		request.getRequestDispatcher("/WEB-INF/login.jsp");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		// TODO vérifier les paramètres
		
		// TODO vérifier que le couple username / password existe bien en base de données
			// SI pas bon retourner 40X ??? correspondant à une erreur d'authentification
		
		String sessionID = UUID.randomUUID().toString();
		// TODO insérer le sessionID en base associé à l'utilisateur / password
		
		Cookie sessionCookie = new Cookie("sessionId", sessionID);
		response.addCookie(sessionCookie);
	}

}
