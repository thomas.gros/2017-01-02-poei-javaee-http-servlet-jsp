package com.aiconoa.trainings.cdi;

import java.io.IOException;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class LampOnOffServlet
 */
@WebServlet("/LampOnOffServlet")
public class LampOnOffServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private static final Logger LOGGER = Logger.getLogger(LampOnOffServlet.class.getName());
	
	@Inject
	private Lamp lamp1;
	
	@Inject
	private Lamp lamp2;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		lamp1.switchOnOff();
		LOGGER.info(String.format("lamp 1 is %s", lamp1.isOn()));
		LOGGER.info(String.format("lamp 2 is %s", lamp2.isOn()));
		
		lamp2.switchOnOff();
		LOGGER.info(String.format("lamp 1 is %s", lamp1.isOn()));
		LOGGER.info(String.format("lamp 2 is %s", lamp2.isOn()));
		
		lamp1.switchOnOff();
		LOGGER.info(String.format("lamp 1 is %s", lamp1.isOn()));
		LOGGER.info(String.format("lamp 2 is %s", lamp2.isOn()));
		
	}


}
