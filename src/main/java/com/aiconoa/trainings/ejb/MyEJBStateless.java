package com.aiconoa.trainings.ejb;

import java.util.Random;

import javax.ejb.Stateless;

/**
 * Session Bean implementation class MyEJBStateless
 */
@Stateless
public class MyEJBStateless {

	public int random() {
		return new Random().nextInt();
	}

}
