package com.aiconoa.trainings.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ListItemServlet
 */
@WebServlet("/ListItemServlet")
public class ListItemServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		List<String> items = new ArrayList<>();
		items.add("flan");
		items.add("tarte aux pommes");
		items.add("moelleux au chocolat");
		items.add("île flottante");
		
		request.setAttribute("items", items);
		
		request.getRequestDispatcher("/WEB-INF/gallery.jsp").forward(request, response);
		
	}

}
