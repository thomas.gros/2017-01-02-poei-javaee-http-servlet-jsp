package com.aiconoa.trainings.servlet;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

@WebServlet("/FileUploadServlet")
@MultipartConfig
public class FileUploadServlet extends HttpServlet {

	private static final Logger LOGGER = Logger.getLogger(FileUploadServlet.class.getName());
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("/WEB-INF/file-upload.jsp")
			   .forward(request, response);
	}
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		LOGGER.info(request.getParameter("title"));
		
		Part filePart = request.getPart("file");
		String fileName = filePart.getSubmittedFileName();
		
		InputStream is = filePart.getInputStream();
		
		File outputFile  = new File("/tmp", fileName);
		Files.copy(is, outputFile.toPath());
		
	}
	
}
