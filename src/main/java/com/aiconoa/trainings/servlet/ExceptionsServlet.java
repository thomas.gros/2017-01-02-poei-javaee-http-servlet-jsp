package com.aiconoa.trainings.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJBException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.aiconoa.trainings.ejb.RecipeService;
import com.aiconoa.trainings.ejb.RecipeServiceException;
import com.aiconoa.trainings.entity.Recipe;

@WebServlet("/ExceptionsServlet")
public class ExceptionsServlet extends HttpServlet {

	private static final Logger LOGGER = Logger.getLogger(ExceptionsServlet.class.getName());
	
	private static final long serialVersionUID = 1L;
 
	@Inject
	private RecipeService recipeService;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {	
		
		Recipe recipe = new Recipe();
		recipe.setTitle("Bonjour je suis un plat");
		recipe.setDescription("Et je suis très bon !");
		recipe.setThumbnail("http://miam.jpg");

//		try {
//			recipeService.createNewRecipe(recipe);
//		} catch(RecipeServiceException e) {
//			LOGGER.log(Level.INFO, "Exception using recipeService EJB", e);
//			
//			Map<String, String> errors = new HashMap<>();
//			if(e.getType()!= null && e.getType().equals("unique")) {
//				errors.put(e.getColumn(), String.format("%s already exists", e.getColumn()));
//			} else {
//				errors.put("global", "an error occured, please try later");
//			}
//		}
	}
}
