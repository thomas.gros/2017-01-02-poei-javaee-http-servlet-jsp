package com.aiconoa.trainings.cdi;

import java.util.Date;

import javax.enterprise.context.Dependent;
import javax.enterprise.event.Event;
import javax.inject.Inject;

@Dependent
public class Lamp {
	private boolean isOn;

	@Inject
	private Event<LampOnOffEvent> eventManager;
	
	public boolean isOn() {
		return isOn;
	}
	
	public void switchOnOff() {
		this.isOn = ! this.isOn;
		
		eventManager.fire(new LampOnOffEvent(this, new Date()));
	}
	
}
