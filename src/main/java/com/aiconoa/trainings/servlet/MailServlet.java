package com.aiconoa.trainings.servlet;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Resource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/MailServlet")
public class MailServlet extends HttpServlet {

	private static final Logger LOGGER = Logger.getLogger(MailServlet.class.getName());
	
	@Resource(name="java:jboss/mail/Gmail")
	private Session session;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		try {
			 
            Message message = new MimeMessage(session);
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse("thomas.gros@aiconoa.com"));
            message.setSubject("test depuis Java EE");
            message.setText("contenu du document");
 
            Transport.send(message);
 
        } catch (MessagingException e) {
        	LOGGER.log(Level.INFO,"Cannot send mail", e);
        }
		// TODO Auto-generated method stub
		LOGGER.info(session.toString());
		super.doGet(req, resp);
	}
}
