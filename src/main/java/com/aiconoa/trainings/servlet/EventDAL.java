package com.aiconoa.trainings.servlet;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.sql.DataSource;

import com.aiconoa.trainings.cdi.RecipeDatabase;

@ApplicationScoped
public class EventDAL {
	
//	@Inject @RecipeDatabase
	private DataSource recipeDS;
	
	// CDI needs no-args constructor to proxify the bean...
	public EventDAL() {}
	
	@Inject
	public EventDAL(@RecipeDatabase DataSource recipeDS) {
		this.recipeDS = recipeDS;
	}
	
	
	public List<Event> findAllEventInDatabase() {
		// recipeDS.getConnection()...
		return null;
	}
	
}
