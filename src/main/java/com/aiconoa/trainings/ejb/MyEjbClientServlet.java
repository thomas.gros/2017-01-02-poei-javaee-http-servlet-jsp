package com.aiconoa.trainings.ejb;

import java.io.IOException;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class MyEjbClientServlet
 */
@WebServlet("/MyEjbClientServlet")
public class MyEjbClientServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private static final Logger LOGGER = Logger.getLogger(MyEjbClientServlet.class.getName());

	@Inject
	private MyEJBStateless myEJBStateless;
	
	@Inject UserService userService;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.getWriter().append("random int: ").append("" + myEJBStateless.random());
	}

}
