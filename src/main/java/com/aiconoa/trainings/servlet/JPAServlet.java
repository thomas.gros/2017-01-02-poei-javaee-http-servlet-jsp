package com.aiconoa.trainings.servlet;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceException;
import javax.persistence.PersistenceUnit;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.aiconoa.trainings.ejb.RecipeService;
import com.aiconoa.trainings.entity.Recipe;

/**
 * Servlet implementation class JPAServlet
 */
@WebServlet("/JPAServlet")
public class JPAServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = Logger.getLogger(JPAServlet.class.getName());

	@Inject
	private RecipeService recipeService;
//	
//	@PersistenceUnit(unitName = "http-jsp-servlet")
//	private EntityManagerFactory emf;
//
//	@Resource
//	private UserTransaction utx;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
//		
//		EntityManager em = null;
//		try {
//			utx.begin();
//			em = emf.createEntityManager();
//			Recipe recipe = em.find(Recipe.class, new Long(1));
//			
//			// updates the recipe ?
//			recipe.setTitle("recette de cuisine");
//			
//			// inserts a new recipe ?
//			Recipe recipe2 = new Recipe();
//			recipe2.setTitle("ma nouvelle recette 2");
//			recipe2.setDescription("la description de ma recette");
//			recipe2.setThumbnail("photo-plat.jpg");
//
//			em.persist(recipe2);
//			utx.commit();
//		} catch (PersistenceException | NotSupportedException | SystemException | IllegalStateException | SecurityException
//				| HeuristicMixedException | HeuristicRollbackException | RollbackException e) {
//			LOGGER.log(Level.INFO, "exception during the transaction", e);
//			try {
//				utx.rollback();
//			} catch (IllegalStateException | SecurityException | SystemException e1) {
//				LOGGER.log(Level.INFO, "error rollbacking the transaction", e);
//			}
//			
//			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
//			return;
//		} finally {
//			if(em != null) {
//				em.close();
//			}
//		}
		
		
		
		
		// recipeService.updateRecipeTitle(1L, "un plat");
		
		Recipe r = recipeService.findById(1L);
		LOGGER.info(String.format("Found recipe %s which author is %s", r, r.getAuthor()));
		
		LOGGER.info(String.format("Found %d recipes", recipeService.countRecipes()));
		

		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

}
