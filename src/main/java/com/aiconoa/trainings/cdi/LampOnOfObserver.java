package com.aiconoa.trainings.cdi;

import java.util.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;

@ApplicationScoped
public class LampOnOfObserver {
	
	private static final Logger LOGGER = Logger.getLogger(LampOnOfObserver.class.getName());

	public void observeLamp(@Observes LampOnOffEvent event) {		
		LOGGER.info(String.format("Received lamp on/off event %s", event));
	}
}
