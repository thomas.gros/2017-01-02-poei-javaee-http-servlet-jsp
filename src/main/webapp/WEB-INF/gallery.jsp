<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="gallery/normalize.css" />
<link rel="stylesheet" href="gallery/gallery.css" />
</head>
<body>
	<h1>Gallery</h1>

	<div class="gallery">
		<c:forEach var="item" items="${items}" varStatus="status">
			<div class="gallery__item">
				<a href="DetailItemServlet?index=${ status.index }">
					<img class="gallery__item__image"
						src="https://dummyimage.com/200x200/000/fff" alt="" />
					<p class="gallery__item__title">${ item }</p>
				</a>
			</div>
		</c:forEach>
	</div>

</body>
</html>