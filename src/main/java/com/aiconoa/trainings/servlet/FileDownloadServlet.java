package com.aiconoa.trainings.servlet;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class FileDownloadServlet
 */
@WebServlet("/FileDownloadServlet")
public class FileDownloadServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
 
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		File file = new File("/Users/thomasgros/Pictures/Wallpapers/angkor_monks.jpg");
		response.setHeader("Content-Type", "image/jpg");
		response.setHeader("Content-Length", String.valueOf(file.length()));		

		Files.copy(Paths.get("/Users/thomasgros/Pictures/Wallpapers/angkor_monks.jpg"), 
				   response.getOutputStream());
		
//		FileInputStream in = new FileInputStream("/Users/thomasgros/Pictures/Wallpapers/angkor_monks.jpg");
//		BufferedInputStream bin = new BufferedInputStream(in);
//		
//		OutputStream out = response.getOutputStream();
//		BufferedOutputStream bout = new BufferedOutputStream(out);
//
////		int b = 0;
////		while((b = in.read()) != -1) {
////			out.write(b);
////		}
//		
//		byte[] buffer = new byte[8192];
//		int l = 0;
//		while((l = bin.read(buffer)) > 0) {
//			bout.write(buffer, 0, l);
//		}
//
//		bout.flush();
//		
//		bin.close();
//		bout.close();
		
	}


}
