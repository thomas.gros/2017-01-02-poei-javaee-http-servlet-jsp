package com.aiconoa.trainings.ejb;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;

import org.hibernate.exception.ConstraintViolationException;

import com.aiconoa.trainings.entity.Recipe;

@Stateless
public class RecipeService {

	@PersistenceContext(unitName = "http-jsp-servlet")
	private EntityManager em;
	
	public void updateRecipeTitle(Long id, String newTitle) {
		Recipe recipe = em.find(Recipe.class, id);
		if(recipe != null) {
			recipe.setTitle(newTitle);
		}
	}
	
	public long countRecipes() {
//		String jpql = "SELECT COUNT(r) FROM Recipe r";
//		TypedQuery<Long> query = em.createQuery(jpql, Long.class);
		TypedQuery<Long> query = em.createNamedQuery("Recipe.countAll", Long.class);
		return query.getSingleResult();
	}

	public Recipe findById(long id) {
		return em.find(Recipe.class, id);
//		
//		String jpql = "SELECT r FROM Recipe r WHERE r.title = :id";
//		TypedQuery<Recipe> query = em.createQuery(jpql, Recipe.class);
//		query.setParameter("id", id);
//		try {
//			return query.getSingleResult();
//		} catch (NoResultException e) {
//			return null;
//		}
	}
	
	public List<Recipe> findAllRecipes() {
		String jpql = "SELECT r FROM Recipe r";
		TypedQuery<Recipe> query = em.createQuery(jpql, Recipe.class);
		query.setFirstResult(10);
		query.setMaxResults(50);
		
		return query.getResultList();
	}
	
	public List<Recipe> findAllByTitle(String title) {
		String jpql = "SELECT r FROM Recipe r WHERE r.title = :title";
		TypedQuery<Recipe> query = em.createQuery(jpql, Recipe.class);
		query.setParameter("title", title);
		
		return query.getResultList();
	}
	
	
//	public Recipe createNewRecipe(Recipe recipe) {
//		// TODO validate pre-conditions	
//		
//		String jpql = "SELECT r FROM Recipe r WHERE r.title  = :title OR r.description = :description";
//		List<Recipe> recipes = em.createQuery(jpql, Recipe.class).getResultList();
//		if(recipes.isEmpty()) {
//			em.persist(recipe);
//			return recipe;
//		}
//		
//		Map<String, String> errors = new HashMap<>();
//		for (Recipe r : recipes) {
//			if(r.getTitle().equals(recipe.getTitle())) {
//				errors.put("title", "title must be unique");
//			}
//			// TODO else if getDescription() ...
//		}
//		throw new RecipeServiceException(errors);
//	}
//		
//	/**
//	 * 
//	 * @param recipe
//	 * @return
//	 * @throws EJBException caused by a NullPointerException if recipe is null
//	 * @throws EJBException caused by a IllegalArgumentException if the recipe's id is not null
//	 * @throws RecipeServiceException if constraint violation is detected on the persistence layer
//	 */
//	public Recipe createNewRecipe(Recipe recipe) {
////		if(recipe == null) {
////			throw new NullPointerException("Recipe must not be null");
////		}
//		
//		// <!---- vérification des préconditions. si non respectées = bug => exception ---->
//		Objects.requireNonNull(recipe, "Recipe must not be null");
//
//		if(recipe.getId() != null) {
//			throw new IllegalArgumentException("recipe's id must be null, found " + recipe.getId());
//		}
//		
//		if(recipe.getTitle() == null) {
//			throw new IllegalArgumentException("recipe's title must not be null");
//		}
//		
//		// <!-- fin de vérification des préconditions --->
//		
//		try {
//			em.persist(recipe);
//			return recipe;
//		} catch(PersistenceException e) {
//			// hibernate
//			if(e.getCause() instanceof ConstraintViolationException) {
//				ConstraintViolationException ex = (ConstraintViolationException) e.getCause();
//				String constraint = ex.getConstraintName(); // title_UNIQUE
//				if(constraint.contains("_UNIQUE")) {
//					String columnName = constraint.substring(0, constraint.indexOf("_UNIQUE"));
//					throw new RecipeServiceException(columnName, "unique", e);
//				} else {
//					throw new RecipeServiceException("unknown", "unknown", e);
//				}
//			} else {
//				throw e;
//			}
//		}
//		
//		
//	}
	
}

