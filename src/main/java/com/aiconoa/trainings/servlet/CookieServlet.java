package com.aiconoa.trainings.servlet;

import java.io.IOException;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class CookieServlet
 */
@WebServlet("/CookieServlet")
public class CookieServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Cookie cookie = new Cookie("some-key", "some-value");
		cookie.setMaxAge(60*60*24*365);
		
		response.addCookie(cookie);
		
		Cookie[] cookies = request.getCookies();
		boolean foundDemoIdCookie = false;
		for (Cookie c : cookies) {
			if(c.getName().equals("demo-id")) {
				foundDemoIdCookie = true;
				break;
			}
		}
	
		if( ! foundDemoIdCookie) {
			Cookie idCookie = new Cookie("demo-id", UUID.randomUUID().toString());
			response.addCookie(idCookie);
		}
	}


}
