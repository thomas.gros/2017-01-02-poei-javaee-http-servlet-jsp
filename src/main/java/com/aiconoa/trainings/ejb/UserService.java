package com.aiconoa.trainings.ejb;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import com.aiconoa.trainings.entity.Recipe;
import com.aiconoa.trainings.entity.User;

@Stateless
public class UserService {

	@PersistenceContext(unitName = "http-jsp-servlet")
	private EntityManager em;
	
	public User findUserbyId(Long id) {
		String jpql = "SELECT u FROM User u FETCH JOIN u.recipes WHERE id = :id";
		TypedQuery<User> query = em.createQuery(jpql, User.class);
		try {
			return query.setParameter("id", id)
						.getSingleResult();
		} catch(NoResultException e) {
			return null;
		}

//		User u = em.find(User.class, id);
//		u.getRecipesAuthored().size();
//		return u;
	}
	
	public List<Recipe> findRecipesByUser(User user) {
		String jpql = "SELECT r FROM Recipe r WHERE author_id = :user";
		TypedQuery<Recipe> query = em.createQuery(jpql, Recipe.class);
		return query.setParameter("user", user.getId())
				.getResultList();
		
	}
	
}
