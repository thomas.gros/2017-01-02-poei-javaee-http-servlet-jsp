package com.aiconoa.trainings.cdi;

import java.io.IOException;
import java.util.logging.Logger;

import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/FireEventServlet")
public class FireEventServlet extends HttpServlet {
	private static final Logger LOGGER = Logger.getLogger(FireEventServlet.class.getName());

	@Inject
	Event<MyEvent> myEvent;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		LOGGER.info("avant d'envoyer l'event");
		
		myEvent.fire(new MyEvent("hello world"));
		
		LOGGER.info("après envoi de l'event");
		
	}
}
