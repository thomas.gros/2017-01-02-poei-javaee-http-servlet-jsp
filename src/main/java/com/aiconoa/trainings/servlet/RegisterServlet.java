package com.aiconoa.trainings.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class RegisterServlet
 */
@WebServlet("/RegisterServlet")
public class RegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RegisterServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// retourne le formulaire
		request.getRequestDispatcher("/WEB-INF/register/form.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		
		Map<String, String> errors = new HashMap<>();

		// validation username
		if(username == null || username.trim().isEmpty()) {
			errors.put("username", "Username must be specified");
		}
		
		// validation password
		if(password == null || password.trim().isEmpty() || password.length() < 4) {
			
			errors.put("password", "Password must be specified and have a length >= 4");
		}
		
		if(! errors.isEmpty()) {
			// valeurs soumises par l'utilisateur
			request.setAttribute("username", username);
			request.setAttribute("password", password);
			
			// erreurs de validation
			request.setAttribute("errors", errors);
			
			request.getRequestDispatcher("/WEB-INF/register/form.jsp").forward(request, response);
			return;
		}

		// sinon => rediriger vers une page de notre choix
		response.sendRedirect("index.html");
		
	}

}
