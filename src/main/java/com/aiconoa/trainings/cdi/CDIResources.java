package com.aiconoa.trainings.cdi;

import javax.annotation.Resource;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.sql.DataSource;

@ApplicationScoped
public class CDIResources {

	@Produces @RecipeDatabase @Resource(lookup="java:jboss/DataSources/RecipeDS")
	private DataSource recipeDS;
}
